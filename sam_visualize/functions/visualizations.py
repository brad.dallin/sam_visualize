"""
visualizations.py 
script contains various pymol visualization functions

CREATED ON: 01/29/2021

AUTHOR(S):
    Bradley C. Dallin (brad.dallin@gmail.com)
    
** UPDATES **

TODO:
"""
##############################################################################
## IMPORTING MODULES
##############################################################################
from pymol import cmd, stored

## GLOBAL
LIGAND_END_GROUPS = {
                      "DOD" : [ "C35", "H36", "H37", "H38" ],
                      "TAM" : [ "N41", "H42", "H43" ],
                      "DAD" : [ "C38", "O39", "N40", "H41", "H42" ],
                      "TOH" : [ "O41", "H42" ],
                      }

##############################################################################
# FUNCTIONS
##############################################################################
## FUNCTION SETTING ATOMS AS SPHERES
def spheres( add_water = True ):
    ## START CLEAN
    cmd.delete( "sele" )
    cmd.hide( "everything" )
    
    ## SET TO ORTHOSCOPIC
    cmd.set( 'orthoscopic' )
    
    ## SET DEPTH CUE
    cmd.set( 'depth_cue', value = 0 )

    ## SET BACKGROUND COLOR TO WHITE
    cmd.bg_color( color = "white" )
    
    ## CREATE SELECTIONS
    cmd.select( "sam", "(not resn SOL)" )
    cmd.select( "water", "(resn SOL)" )

    ## SET ATOMS REPRESTATION AS SPHERES
    cmd.show( representation = "spheres" )
        
    ## HIDE WATERS IF FALSE
    if add_water is not True:
        cmd.hide( "(resn SOL)" )
    
    ## REMOVE DUMMY ATOMS
    cmd.remove( "(name MW)" )

    ## CHANGE TO MORE APPEALING COLOR PATTERN
    # LIGANDS
    cmd.set( "sphere_scale", value = 0.8, selection = "sam" )
    cmd.color( "yellow", selection = "(elem S)" )
    cmd.color( "grey", selection = "(elem C)" )
    cmd.color( "red", selection = "(not resn SOL and elem O)" )
    cmd.color( "blue", selection = "(elem N)" )
    cmd.color( "white", selection = "(not resn SOL and elem H)" )
    
    # WATER
    cmd.set( "sphere_scale", value = 0.6, selection = "water" )
    cmd.color( "red", selection = "(name OW)" )
    cmd.color( "white", selection = "(name HW)" )
    
    # COUNTERIONS
    cmd.color( "green", selection = "(elem CL)" )

## FUNCTION TO VIEW SAM FROM SIDE
def sideview_spheres( add_water = True, translate = [ 0, 0, 0 ] ):
    ## SET AS SPHERES
    spheres( add_water = False )

    ## REPRESENT WATER AS CARTOON
    cmd.show( representation = "sticks", selection = "(resn SOL)" )

    ## SET REPRESENTATION REPRESENTATION
    cmd.set( "stick_radius", value = 1.0 )

    ## HIDE WATERS NOT IN SLICE
    cmd.hide( "(resn SOL and (y>45 or y<25))" )

    ## SLIDE WATERS TO COVER TOP OF SAM
    cmd.translate( translate, selection = "(resn SOL)" )
    
    ## ROTATE X
    cmd.rotate( "x", angle = 270, selection = "all" )

## FUNCTION TO VIEW SAM FROM TOP
def topview_spheres( translate = [ 0, 0, 0 ] ):
    ## SET AS SPHERES
    spheres( add_water = False )
    
    ## SLIDE DOWN (TOP ATOMS CUT OFF)
    cmd.translate( translate )

    ## MAKE SAM TRANSPARENT
    cmd.set( "sphere_transparency", value = 0.4 )

    ## ACCENT END GROUP ATOMS
    for group, atoms in LIGAND_END_GROUPS.items():
        ## LOOP THROUGH ATOM NAMES
        for name in atoms:
            ## MAKE ATOM OPAQUE
            cmd.set( "sphere_transparency", value = 0.0,
                     selection = "(resn {} and name {})".format( group, name ) )

## FUNCTION TO VIEW SAM FROM SIDE
def ligandview_spheres():
    ## SET AS SPHERES
    spheres( add_water = False )
    
    ## ROTATE Z & Y
    cmd.rotate( "z", angle = 180 )
    cmd.rotate( "y", angle = 90 )

def coord6_waters( frame, target_resn = None, rotate = 0 ):
    # SET AS SPHERES
    spheres( add_water = False )

    ## VISUALIZE TARGET WATERS
    if target_resn is not None:
        for key, rn in target_resn.items():
            ## CREATE SELECTIONS
            input_string = "resi " + "+".join( [ str(nn+1) for nn in rn ] )
            # input_string += " and not elem H"
            cmd.select( key, input_string )
            # cmd.show( representation = "sphere", selection = key )
            cmd.show( representation = "sticks", selection = key )

    ## SET REPRESENTATION REPRESENTATION
    cmd.set( "stick_radius", value = 1.0 )
    
    ## MOVE TO TARGET FRAME (ASSUMES TRAJ IS LOADED)
    cmd.frame( frame )

    ## ROTATE X
    cmd.rotate( "x", angle = rotate, selection = "all" )

## MAKE FUNCTIONS AVAILABLE ON PYMOL GUI
## SPHERES FUNCTION
cmd.extend( "visualize_as_spheres", spheres )

## SIDE VIEW SPHERES FUNCTION
cmd.extend( "visualize_sideview_as_spheres", sideview_spheres )

## TOP VIEW SPHERES FUNCTION
cmd.extend( "visualize_topview_as_spheres", topview_spheres )

## LIGAND VIEW SPHERES FUNCTION
cmd.extend( "visualize_ligandview_as_spheres", ligandview_spheres )

## COORD6 FUNCTION
cmd.extend( "coord6_waters", coord6_waters )
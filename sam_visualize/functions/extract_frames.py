"""
extract_frames.py 
script contains functions to extract specific frames/configurations from an MD simulation

CREATED ON: 03/05/2021

AUTHOR(S):
    Bradley C. Dallin (brad.dallin@gmail.com)
    
** UPDATES **

TODO:
"""
##############################################################################
## IMPORTING MODULES
##############################################################################
## IMPORT OS
import os
## IMPORT NUMPY
import numpy as np
## IMPORT MDTRAJ
import mdtraj as md
## IMPORT TRAJECTORY FUNCTION
from sam_analysis.core.trajectory import load_md_traj
## FUNCTION TO SAVE AND LOAD PICKLE FILES
from sam_analysis.core.pickles import load_pkl

##############################################################################
# FUNCTIONS
##############################################################################
## FUNCTION TO EXTRACT FRAMES WITH WATER COORDINDATION = 6
def coord_6_frames( sim_working_dir, input_prefix, coord_file, n_neighbors = 6, cutoff = 0.33, idx = None ):
    """function that extracts coord=6 frames and configurations"""
    def _single_frame( traj_frame, data, frame, water_indices, n_neighbors ):
        """function to compute for a single frame"""
        ## CREATE PLACEHOLDER
        results = {}
        
        ## TARGET PER FRAME
        target_frame_mask   = data[:,0] == frame
        target_coords_frame = data[ target_frame_mask, 1 ]

        ## LOOP THROUGH INDEX
        for ii in range(len(target_coords_frame)):
            ## COMPUTE ATOM-ATOM CARTESIAN DISTANCES FOR FIRST OXYGEN
            atom_pairs = np.array([ [ aa, target_coords_frame[ii] ] 
                                      for aa in water_indices ])
            dist_vector = md.compute_displacements( traj_frame, 
                                                    atom_pairs = atom_pairs,
                                                    periodic   = True )
            ## REDUCE EMPTY DIMENSION
            dist_vector = dist_vector.squeeze()

            ## COMPUTE VECTOR MAGNITUDES
            magnitudes = np.sqrt( np.sum( dist_vector**2., axis = 1 ) )
                    
            ## SORT NEIGHBORS AND KEEP N NEAREST
            nn = n_neighbors+1
            nearest_neighbors = magnitudes.argsort()[1:nn]

            ## NEIGHBOR INDICES
            key = "f{}_c{}".format( frame+1, ii )
            rr  = [ traj_frame.topology.atom(int(target_coords_frame[ii])).residue.index ]
            rr += [ traj_frame.topology.atom(water_indices[int(aa)]).residue.index 
                    for aa in nearest_neighbors ]
            
            ## APPEND
            results[key] = rr

        ## RETURN RESULTS
        return results
    ## PATH TO COORDINATION DATA
    path_pkl = os.path.join( sim_working_dir, "output_files", coord_file )

    ## LOAD TRAJECTORY
    traj = load_md_traj( path_traj    = sim_working_dir,
                         input_prefix = input_prefix )

    ## GET ATOM TYPES
    atoms = [ atom for atom in traj.topology.atoms
              if atom.element.symbol in [ "N", "O" ] ]
    water_atoms = [ atom for atom in atoms 
                    if atom.residue.name in [ "SOL", "HOH" ] ]
    water_indices = np.array([ atom.index for atom in water_atoms ])

    ## LOAD COORD DATA
    data = load_pkl( path_pkl )
        
    ## UNPACK COORDINATION
    coord = data[0][0]
    for dd in data[1:]:
        coord += dd[0]

    ## CONVERT TO NUMPY ARRAY
    coord = np.array(coord)

    ## GET FRAMES WITH COORD=6 [ frame, num, ww_num, ss_num, ii, is_target ]
    # coord_6_mask = coord[:,2] == 6
    coord_6_mask = np.logical_and( np.isclose( coord[:,2], 6. ), coord[:,-1] > 0 )
    coord_6 = coord[coord_6_mask,:][:,[0,4]]

    ## COMPUTE COORD=6 PROBABILITY
    prob = coord_6.shape[0] / coord.shape[0]
    print( "p(coord=6) = {:.3e}".format( prob ) )

    if idx is not None:
        ## COMPUTE SINGLE FRAME
        print( "COMPUTING FRAME: {}".format( idx ) )
        results = _single_frame( traj[idx], coord_6, idx, water_indices, n_neighbors )
    else:
        ## LOOP THROUGH FRAMES
        results = []
        for ii in range(traj.n_frames):
            ## PRINT PROGRESS
            if ii % 10 == 0:
                print( "WORKING ON FRAME: {} OF {}".format( ii, traj.time.size ) )
            ## COMPUTE SINGLE FRAME
            results.append( _single_frame( traj[ii], coord_6, ii, water_indices, n_neighbors ) )
    
    ## RETURN RESULTS
    return results
    